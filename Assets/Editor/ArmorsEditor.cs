﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DailyRPG;

[CustomEditor(typeof(Armors))]
public class ArmorsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("ELŐRE TÁROLT ELEMEK! - commit test", MessageType.Warning);
        DrawDefaultInspector();
    }
}