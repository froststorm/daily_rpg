﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DailyRPG;

[CustomEditor(typeof(Weapons))]
public class WeaponsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("ELŐRE TÁROLT ELEMEK!", MessageType.Warning);
        DrawDefaultInspector();
    }
}