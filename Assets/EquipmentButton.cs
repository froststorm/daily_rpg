﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DailyRPG
{
    public class EquipmentButton : MonoBehaviour
    {
        public Item myItem;

        public void Dearm()
        {
            if (myItem != null)
            {
                Hero.getInstance.equipment.RemoveItemOnEquipment(myItem, Callback);
            }
            else
            {
                Debug.Log("Null Ref Exception...");
            }
        }

        private void Callback()
        {
            transform.GetChild(0).GetComponent<Text>().text = "- EMPTY -";
            if(myItem.getArmorType() == ArmorType.GLOVES)
            {
                GameObject.Find("Gloves1").transform.GetChild(0).GetComponent<Text>().text = "- EMPTY -";
                GameObject.Find("Gloves2").transform.GetChild(0).GetComponent<Text>().text = "- EMPTY -";

                GameObject.Find("Gloves1").GetComponent<Image>().color = Color.white;
                GameObject.Find("Gloves2").GetComponent<Image>().color = Color.white;

            }
            GetComponent<Image>().color = Color.white;
        }
    }
}
