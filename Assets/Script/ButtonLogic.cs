﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DailyRPG;
using UnityEngine.UI;

public class ButtonLogic : MonoBehaviour
{
    public List<GameObject> obj = new List<GameObject>();
    public GameObject inv;
    bool t;

    public void InventoryShow(bool value)
    {
        inv.SetActive(value);
        Hero.getInstance.UIRefresh();
    }
    public void Adder(int id)
    {
        if (Hero.getInstance.eloszt <= 0)
            return;

        Hero.HeroProperties hero = Hero.getInstance.getHeroProperties();

        switch (id)
        {
            case 0:
                hero.strength++;
                break;
            case 1:
                hero.agility++;
                break;
            case 2:
                hero.dexterity++;
                break;
            case 3:
                hero.stamina++;
                break;
            case 4:
                hero.wisdom++;
                break;
            case 5:
                hero.intelligency++;
                break;

        }
        Hero.getInstance.eloszt--;
        Hero.getInstance.heroProperties = hero;
        Hero.getInstance.UIRefresh();

    }
    
    public void LevelUp()
    {
        Hero.getInstance.leveling.LevelUp(Hero.getInstance);
        Hero.getInstance.UIRefresh();
    }   

    public void AddRandomItem()
    {
        Hero hero = Hero.getInstance;
        List<ArmorItem> items = hero.armors.set[Random.Range(0, hero.armors.set.Count)].getArmorItems();
        hero.inventory.UploadItem((Item)items[Random.Range(0,items.Count)]);

        RefreshInventoryUI();
    }

    public void RefreshInventoryUI()
    {
        Clear();

        Hero hero = Hero.getInstance;
        GameObject clone = (GameObject)Resources.Load("Button");
        Transform parent = GameObject.Find("Inventory").transform;

        for (int x = 0; x < hero.inventory.items.Count; x++)
        {
            GameObject cl = (GameObject)Instantiate(clone);
            obj.Add(cl);

            cl.transform.SetParent(parent.transform);
            cl.GetComponent<ButtonScript>().myItem = hero.inventory.items[x];
            Text text = cl.transform.GetChild(0).GetComponent<Text>();
            text.text = hero.inventory.items[x].getName();
        }
    }
    void Clear()
    {
        for(int x = 0; x < obj.Count; x++)
        {
            Destroy(obj[x]);
        }

        obj.Clear();
    }
}
