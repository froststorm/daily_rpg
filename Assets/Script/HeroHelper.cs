﻿using System.Collections.Generic;
using UnityEngine;

namespace DailyRPG
{
    public class HeroHelper : MonoBehaviour
    {
        #region ARMORS - WEAPONS
        private Armors _armors;
        public Armors armors
        {
            get
            {
                if(_armors == null)
                {
                    _armors = FindObjectOfType<Armors>();
                }

                return _armors;
            }
        }

        private Weapons _weapons;
        public Weapons weapons
        {
            get
            {
                if (_weapons == null)
                {
                    _weapons = FindObjectOfType<Weapons>();
                }

                return _weapons;
            }
        }
        #endregion

        #region FUNCTIONS

        public class Leveling
        {
            int XpToLevelUp;
            int previous;
            int currentXp;

            public void LevelUp(Hero hero)
            {
                hero.level++;
                hero.eloszt += 5;

                Debug.Log("LevelUp!");
            }
            public void ScanHeroLevel(Hero hero)
            {
                if (hero.level == 1)
                {
                    XpToLevelUp = 100;
                }
                else
                {
                    XpToLevelUp = (int)(previous * 1.2f) + 100;
                }

                if (currentXp > XpToLevelUp)
                {
                    previous = XpToLevelUp;
                    hero.level++;
                    hero.eloszt += 5;
                    XpToLevelUp = (int)(previous * 1.2f) + 100;
                    int cur = currentXp;
                    currentXp = cur - XpToLevelUp;
                }


                if (currentXp == XpToLevelUp)
                {
                    previous = XpToLevelUp;
                    hero.level++;
                    hero.eloszt += 5;
                    XpToLevelUp = (int)(previous * 1.2f) + 100;
                    currentXp = 0;
                }
            }
        }

        public class Inventory
        {
            public List<Item> items = new List<Item>();

            public void UploadItem(Item item)
            {
                items.Add(item);
            }
            public void PrintItem(int count)
            {
                Debug.Log(items[count].getName());
            }

            #region GETTERS
            public List<Item> getInventoryItemsList()
            {
                return items;
            }
            public Dictionary<string,Item> getInventoryItemsDictionary()
            {
                Dictionary<string, Item> dictionary = new Dictionary<string, Item>();

                foreach(Item item in items)
                {
                    dictionary.Add(item.getName(),item);
                }

                return dictionary;
            }
            #endregion
        }

        [System.Serializable]
        public class Equipment
        {

            public ArmorItem helmet { get; set; }
            public ArmorItem shoulders { get; set; }
            public ArmorItem chest { get; set; }
            public ArmorItem belt { get; set; }
            public ArmorItem gloves { get; set; }
            public ArmorItem leg { get; set; }
            public ArmorItem boots { get; set; }

            public Weapon weapon;
            
            
            private void AddStats(Item item)
            {
                Hero hero = Hero.getInstance;

                hero.heroData.rangedDefense += hero.heroData.allDefense + item.getRangedDefense();
                hero.heroData.meleeDefense += hero.heroData.allDefense + item.getMeleeDefense();
                hero.heroData.magicalDefense += hero.heroData.allDefense + item.getMagicalDefense();
                hero.addLife += item.getLife();
                Debug.Log("Add: " + hero.heroData.rangedDefense + " - " + hero.heroData.meleeDefense + " - " + hero.heroData.magicalDefense);
            }
            private void MinusStats(Item item)
            {
                Hero hero = Hero.getInstance;

                hero.heroData.rangedDefense -= hero.heroData.allDefense + item.getRangedDefense();
                hero.heroData.meleeDefense -= hero.heroData.allDefense + item.getMeleeDefense();
                hero.heroData.magicalDefense -= hero.heroData.allDefense + item.getMagicalDefense();
                hero.addLife -= item.getLife();

            }
            private void ChangeStats(Item from, Item to)
            {
                Hero hero = Hero.getInstance;

                hero.heroData.rangedDefense += hero.heroData.allDefense + to.getRangedDefense();
                hero.heroData.meleeDefense += hero.heroData.allDefense + to.getMeleeDefense();
                hero.heroData.magicalDefense += hero.heroData.allDefense + to.getMagicalDefense();
                hero.addLife += to.getLife();

                hero.heroData.rangedDefense -= (hero.heroData.allDefense + from.getRangedDefense());
                hero.heroData.meleeDefense -= (hero.heroData.allDefense + from.getMeleeDefense());
                hero.heroData.magicalDefense -= (hero.heroData.allDefense + from.getMagicalDefense());
                hero.addLife -= from.getLife();

                Debug.Log("Change: " + hero.heroData.rangedDefense + " - " + hero.heroData.meleeDefense + " - " + hero.heroData.magicalDefense);
            }

            private void ChangeItem(Item item, Item to)
            {
                if (item.getCast() == to.getCast())
                {
                    ChangeStats(item, to);
                    item = to;
                }
            }
            public void AddItemToEquipment(Item item, System.Action callback)
            {
                if(item.getItemType() == Type.ARMOR)
                {
                    ArmorItem armor = (ArmorItem)item;

                    if (armor.CheckCast(armor.getClass()))
                    {
                        switch(item.getArmorType())
                        {
                            case ArmorType.BELT:
                                if (belt == null)
                                {
                                    belt = armor;
                                    AddStats((Item)belt);
                                }
                                else
                                    ChangeItem((Item)belt, (Item)armor);
                                break;

                            case ArmorType.BOOTS:
                                if (boots == null)
                                {
                                    boots = armor;
                                    AddStats((Item)boots);
                                }
                                else
                                    ChangeItem((Item)boots, (Item)armor);
                                break;

                            case ArmorType.CHEST:
                                if (chest == null)
                                {
                                    chest = armor;
                                    AddStats((Item)chest);
                                }
                                else
                                    ChangeItem((Item)chest, (Item)armor);
                                break;

                            case ArmorType.GLOVES:
                                if (gloves == null)
                                {
                                    gloves = armor;
                                    AddStats((Item)gloves);
                                }
                                else
                                    ChangeItem((Item)gloves, (Item)armor);
                                break;

                            case ArmorType.HELMET:
                                if (helmet == null)
                                {
                                    helmet = armor;
                                    AddStats((Item)helmet);
                                }
                                else
                                    ChangeItem((Item)helmet, (Item)armor);
                                break;

                            case ArmorType.LEG:
                                if (leg == null)
                                {
                                    leg = armor;
                                    AddStats((Item)leg);
                                }
                                else
                                    ChangeItem((Item)leg, (Item)armor);
                                break;

                            case ArmorType.SHOULDERS:
                                if (shoulders == null)
                                {
                                    shoulders = armor;
                                    AddStats((Item)shoulders);
                                }
                                else
                                    ChangeItem((Item)shoulders, (Item)armor);
                                break;
                        }

                        Debug.Log(armor.getName() + " equiped!");
                        callback();

                    }
                    else
                    {
                        Debug.Log("You do not wear it!");
                    }
                }
                else if(item.getItemType() == Type.WEAPON)
                {
                    Weapon weapon = (Weapon)item;
                }
            }
            public void RemoveItemOnEquipment(Item item, System.Action callback)
            {
                switch (item.getArmorType())
                {
                    case ArmorType.BELT:
                        belt = null;
                        break;

                    case ArmorType.BOOTS:
                        boots = null;
                        break;

                    case ArmorType.CHEST:
                        chest = null;
                        break;

                    case ArmorType.GLOVES:
                        gloves = null;
                        break;

                    case ArmorType.HELMET:
                        helmet = null;
                        break;

                    case ArmorType.LEG:
                        leg = null;
                        break;

                    case ArmorType.SHOULDERS:
                        shoulders = null;
                        break;
                }

                MinusStats(item);
                Debug.Log("Removed from the Equipment: " + item.getName());
                Debug.Log("Remove: " + Hero.getInstance.heroData.rangedDefense + " - " + Hero.getInstance.heroData.meleeDefense + " - " + Hero.getInstance.heroData.magicalDefense);

                callback();
            }

            #region GETTERS
            public List<Item> getEquipmentItemsList()
            {
                List<Item> items = new List<Item>() { (Item)helmet, (Item)shoulders, (Item)chest, (Item)belt, (Item)gloves, (Item)leg, (Item)boots };
                return items;
            }
            public Dictionary<string,Item> getEquipmentItemsDictionary()
            {
                Dictionary<string, Item> items = new Dictionary<string, Item>();

                items.Add("helmet", (Item)helmet);
                items.Add("shoulders", (Item)shoulders);
                items.Add("chest", (Item)chest);
                items.Add("belt", (Item)belt);
                items.Add("gloves", (Item)gloves);
                items.Add("leg", (Item)leg);
                items.Add("boots", (Item)boots);

                return items;
            }
            #endregion
        }
        #endregion
    }
}
