﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DailyRPG;
using UnityEngine.UI;

public class ValueChanger : MonoBehaviour
{
    public void Value(Dropdown dr)
    {
        switch(dr.value)
        {
            case 0:
                Hero.getInstance.heroClass = HeroClass.WARRIOR;
                break;

            case 1:
                Hero.getInstance.heroClass = HeroClass.DEFENDER;
                break;

            case 2:
                Hero.getInstance.heroClass = HeroClass.ROGUE;
                break;

            case 3:
                Hero.getInstance.heroClass = HeroClass.HUNTER;
                break;

            case 4:
                Hero.getInstance.heroClass = HeroClass.MAGE;
                break;

            case 5:
                Hero.getInstance.heroClass = HeroClass.PRIEST;
                break;
        }

        Hero.getInstance.UIRefresh();
    }
}
