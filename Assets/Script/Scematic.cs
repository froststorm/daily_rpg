﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyRPG
{
    public class Scematic
    {
        public string _name { private get; set; }

        public int _price { private get; set; }

        public Scematic(string name, int price)
        {
            this._name = name;
            this._price = price;
        }

        public string getName()
        {
            return _name;
        }
        public int getPrice()
        {
            return _price;
        }
        public int getSellPrice()
        {
            return _price / 5;
        }

    }
}