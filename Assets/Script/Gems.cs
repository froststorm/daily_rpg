﻿using UnityEngine;
using System.Collections;

public class Gems : MonoBehaviour {
	public void AddGem (string typeOfGem, int levelOfEnemy)
	{
		int bonusStatByGem;
		int GemLevel = levelOfEnemy;
		if (typeOfGem == "Strength") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Agility") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Dexterity") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Stamina") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Wisdom") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Intelligency") {
			bonusStatByGem = 1 + (int)(0.5f * GemLevel);
		}
		if (typeOfGem == "Critical") {
			bonusStatByGem = Random.Range (1, 20); //Ezt is lehet használni a bónusz értéknek.
		}
	}
}
