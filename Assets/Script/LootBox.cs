﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DailyRPG
{
    public enum DropType
    {
        ENEMY = 0,
        BOSS = 1
    }
    public enum ChestType
    {
        COMMON_SIMPLE = 0,
        RARE_SIMPLE = 1,
        VERY_RARE_SIMPLE = 2,

        COMMON_BOSS = 3,
        RARE_BOSS = 4,
        VERY_RARE_BOSS = 5
    }
    public class LootBox : MonoBehaviour
    {
        public string _name = "LOOTBOX";
        public DropType type;
        public ChestType chestType;

        public string getName()
        {
            return _name;
        }

        public Armors armors;
        public Weapons weapons;

        void Awake()
        {
            armors = FindObjectOfType<Armors>();
            weapons = FindObjectOfType<Weapons>();
        }

        public void GenerateItem()
        {
            Item item = getRandomItem();
        }
        public DropType getType()
        {
            System.Random r = new System.Random();

            int r1 = r.Next(1);

            type = (DropType)r1;
            return type;
        }
        public ChestType getChestType()
        {
            if(getType() == DropType.ENEMY)
            {
                System.Random r = new System.Random();

                int r1 = r.Next(2);
                chestType = (ChestType)r1;

                return chestType;
            }
            else
            {
                System.Random r = new System.Random();

                int r1 = r.Next(2, 5);
                chestType = (ChestType)r1;

                return chestType;
            }
        }
        private Item getRandomItem()
        {
            System.Random random = new System.Random();

            int r1 = random.Next(1);
            Item item = null;
            if (r1 == 0)
            {
                //ARMOR
                int r2 = random.Next(armors.set.Count);
                int r3 = random.Next(armors.set[0].getArmorItems().Count);
                List<ArmorItem> items = new List<ArmorItem>();

                items = armors.set[r2].getArmorItems();

                item = (Item)items[r3];
            }
            else if(r1 == 1)
            {
                //WEAPON
                int r2 = random.Next(weapons.weapons.Count);
                List<Weapon> items = new List<Weapon>();
                
                items = weapons.weapons;

                item = (Item)items[r2];
            }

            return item;
        }
    }
}