﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyRPG
{
    public enum Type
    {
        WEAPON,
        ARMOR
    }

    public enum ItemClass
    {
        WARRIOR_DEFENDER,
        ROGUE_HUNTER,
        MAGE_PRIEST
    }

    public enum Rarity
    {
        COMMON = 0,
        UNCOMMON = 1,
        HEROIC = 2,
        EPIC = 3,
        LEGENDARY = 4
    }

    public class Item
    {
        public string _name { get; set; }
        public int _price { get; set; }
        public Rarity rarity { get; set; }
        public int _gemSlot { get; set; }
        public Scematic _scematic { get; set; }


        #region ARMOR

        [Header("Life Values: ")]
        public int _life = 0;
        public float _lifeMul = 0.0f;

        [Header("Select Type: ")]
        public ArmorType type;

        [Header("Defense Values: ")]
        public int _meleeDefense = 0;
        public int _rangedDefense = 0;
        public int _magicalDefense = 0;

        public ItemClass itemClass;

        #endregion

        #region WEAPON
        [Header("Select Cast: ")]
        public WeaponCast weaponCast;

        [Header("Select Weapon Type: ")]
        public WeaponType wtype;

        [Header("Select Hand Status: ")]
        public Hand hand;

        [Header("Damage Values: ")]
        public int _minDamage = 0;
        public int _maxDamage = 0;

        [Header("Damage Multiple Values: ")]
        public int _minDamageM = 0;
        public int _maxDamageM = 0;

        [Header("Healing Values: ")]
        public float _minHeal;
        public float _maxHeal;

        [Header("Healing Multipler Values: ")]
        public float _minHealM;
        public float _maxHealM;

        [Header("Attack Speed: ")]
        public float _attackSpeed = 0.0f;
        #endregion

        #region CONSTRUCTORS
        public Item(ItemClass itemClass,Type itemType,string name, int price, int life, Scematic scematic, float lifeMul, ArmorType type, int meleeDef, int rangedDef, int magicDef)
        {
            this.itemClass = itemClass;
            this.itemType = itemType;
            this._name = name;
            this._price = price;
            this._scematic = scematic;
            this._life = life;
            this._lifeMul = lifeMul;
            this.type = type;

            this._meleeDefense = meleeDef;
            this._rangedDefense = rangedDef;
            this._magicalDefense = magicDef;

        }
        public Item(Type itemType,string name, int price, Scematic scematic, WeaponCast cast, WeaponType type, Hand hand, int minD, int maxD, float minH, float maxH)
        {
            this.itemType = itemType;
            this._name = name;
            this._price = price;
            this._scematic = scematic;
            this.weaponCast = cast;
            this.wtype = type;
            this.hand = hand;
            this._minDamage = minD;
            this._maxDamage = maxD;
            this._minHeal = minH;
            this._maxHeal = maxH;
        }
        #endregion

        #region GETTERS
        public string getName()
        {
            return _name;
        }
        public int getPrice()
        {
            return _price;
        }
        public int getSellPrice()
        {
            return _price / 5;
        }
        public int getGemSlot()
        {
            return _gemSlot;
        }
        public Rarity getRarityType()
        {
            return rarity;
        }

        //ARMOR
        public ItemClass getItemClass()
        {
            return itemClass;
        }
        public int getLife()
        {
            return _life;
        }
        public float getLifeMul()
        {
            return _lifeMul;
        }

        public int getMeleeDefense()
        {
            return _meleeDefense;
        }
        public int getRangedDefense()
        {
            return _rangedDefense;
        }
        public int getMagicalDefense()
        {
            return _magicalDefense;
        }
        public ArmorType getArmorType()
        {
            return type;
        }
        //ARMOR

        //WEAPON
        public WeaponCast getCast()
        {
            return weaponCast;
        }
        public WeaponType getType()
        {
            return wtype;
        }
        public Hand getHandType()
        {
            return hand;
        }

        public int getMinDamage()
        {
            return _minDamage;
        }
        public int getMaxDamage()
        {
            return _maxDamage;
        }
        public float getMinHeal()
        {
            return _minHeal;
        }
        public float getMaxHeal()
        {
            return _maxHeal;
        }
        public float getAttackSpeed()
        {
            return _attackSpeed;
        }
        //WEAPON

        public Scematic getScematic()
        {
            return _scematic;
        }

        public Type itemType { private get; set; }
        public Type getItemType()
        {
            return itemType;
        }
        #endregion

        #region CONVERSION
        public static explicit operator Item(ArmorItem d)
        {
            return new Item(d.getClass(),d.getItemType(),d.getName(), d.getPrice(), d.getLife(), d.getScematic(), d.getLifeMul(), d.getType(), d.getMeleeDefense(), d.getRangedDefense(), d.getMagicalDefense());
        }
        public static explicit operator Item(Weapon d)
        {
            return new Item(d.getItemType(),d.getName(), d.getPrice(), d.getScematic(), d.getCast(), d.getType(), d.getHandType(), d.getMinDamage(), d.getMaxDamage(), d.getMinHeal(), d.getMaxHeal());
        }
        public static explicit operator ArmorItem(Item d)
        {
            return new ArmorItem(d.getItemClass(),d.getName(),d.getLife(),d.getLifeMul(),d.getArmorType(),d.getMeleeDefense(),d.getRangedDefense(),d.getMagicalDefense(),d.getMeleeDefense(),d.getRangedDefense(),d.getMagicalDefense());
        }
        public static explicit operator Weapon(Item d)
        {
            return new Weapon(d.getName(), d.getPrice(), d.getScematic(), d.getCast(), d.getType(), d.getHandType(), d.getMinDamage(), d.getMaxDamage(), d.getMinHeal(), d.getMaxHeal());
        }
        #endregion
    }
}
