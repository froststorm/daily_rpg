﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace DailyRPG
{
    public enum HeroClass
    {
        WARRIOR,
        DEFENDER,
        ROGUE,
        HUNTER,
        MAGE,
        PRIEST
    }

    public class Hero : HeroHelper
    {
        public static Hero getInstance { get; private set; }

        public int addLife;
        #region HERO DATA

        public struct HeroProperties
        {
            public int strength { get; set; }
            public int agility { get; set; }
            public int dexterity { get; set; }
            public int stamina { get; set; }
            public int wisdom { get; set; }
            public int intelligency { get; set; }

        }

        public struct HeroData
        {
            public int damage { get; set; }
            public int hitChance { get; set; }
            public float attackSpeed { get; set; }
            public int criticalCance { get; set; }
            public int dodge { get; set; }
            public int life { get; set; }
            public int lifeReg { get; set; }
            public int allDefense { get; set; }
            public int mana { get; set; }
            public int manaReg { get; set; }
            public int healing { get; set; }
            public int meleeDefense { get; set; }
            public int rangedDefense { get; set; }
            public int magicalDefense { get; set; }
        }
        #endregion

        #region HERO CLASS
        [Header("Hero Class")]
        public HeroClass heroClass;

        public HeroClass getHeroClass()
        {
            return heroClass;
        }
        public string getHeroClassToString()
        {
            switch(getHeroClass())
            {
                case HeroClass.DEFENDER:
                    return "Defender";
                case HeroClass.HUNTER:
                    return "Hunter";
                case HeroClass.MAGE:
                    return "Mage";
                case HeroClass.PRIEST:
                    return "Priest";
                case HeroClass.ROGUE:
                    return "Rogue";
                case HeroClass.WARRIOR:
                    return "Warrior";
                default:
                    return "Please, select class!";
            }
        }
        #endregion

        #region HERO DATA ON INSPECTOR DISPLAY

        [Header("Main Data")]
        public string heroName;
        public int level;
        public int eloszt;

        public Hero.HeroProperties heroProperties = new HeroProperties();
        public Hero.HeroData heroData = new HeroData();

        public HeroData getHeroData()
        {
            return heroData;
        }
        public HeroProperties getHeroProperties()
        {
            return heroProperties;
        }

        public static HeroData setHeroData(HeroData value)
        {
            return value;
        }
        public static HeroProperties setHeroProperties(HeroProperties value)
        {
            return value;
        }
        #endregion

        #region INHERITED CLASSES

        private Leveling _leveling;
        public Leveling leveling
        {
            get
            {
                if (_leveling == null)
                    _leveling = new Leveling();

                return _leveling;
            }
        }

        private Inventory _inventory;
        public Inventory inventory
        {
            get
            {
                if (_inventory == null)
                    _inventory = new Inventory();

                return _inventory;
            }
        }

        private Equipment _equipment;
        public Equipment equipment
        {
            get
            {
                if (_equipment == null)
                    _equipment = new Equipment();

                return _equipment;
            }
        }

        #endregion

        #region UI
        [Tooltip("Csak a kiiratáshoz!")][Header("UI")]
        public Text textProperties;
        public Text textData;
        #endregion

        void Awake()
        {
            getInstance = this;
        }
        void Start()
        {
            HeroInitSet();
            UIRefresh();
        }
        public void HeroInitSet()
        {
            switch (heroClass)
            {
                case HeroClass.WARRIOR:
                    heroProperties.strength = 15;
                    heroProperties.agility = 10;
                    heroProperties.dexterity = 0;
                    heroProperties.stamina = 10;
                    heroProperties.wisdom = 0;
                    heroProperties.intelligency = 0;
                    heroData.damage = (3 * heroProperties.strength) + (2 * heroProperties.agility);
                    heroData.hitChance = (10 * heroProperties.strength) + (12 * heroProperties.agility) + (3 * heroProperties.dexterity) + (2 * heroProperties.wisdom);
                    heroData.attackSpeed = 15 * heroProperties.strength;
                    heroData.criticalCance = (10 * heroProperties.agility) + (1 * heroProperties.intelligency);
                    heroData.dodge = 0;
                    heroData.life = 15 * heroProperties.stamina;
                    heroData.lifeReg = (int)0.4f * heroProperties.stamina;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.DEFENDER:
                    heroProperties.strength = 15;
                    heroProperties.agility = 0;
                    heroProperties.dexterity = 10;
                    heroProperties.stamina = 15;
                    heroProperties.wisdom = 0;
                    heroProperties.intelligency = 0;
                    heroData.damage = (int)2 * heroProperties.strength;
                    heroData.hitChance = (8 * heroProperties.strength) + (2 * heroProperties.agility);
                    heroData.attackSpeed = 10 * heroProperties.strength;
                    heroData.criticalCance = 0;
                    heroData.dodge = (20 * heroProperties.dexterity) + (3 * heroProperties.wisdom);
                    heroData.life = (15 * heroProperties.dexterity) + (20 * heroProperties.stamina);
                    heroData.lifeReg = (int)((1.5f * heroProperties.dexterity) + (1 * heroProperties.stamina) + (0.1f * heroProperties.intelligency));
                    heroData.lifeReg = (int)((1.5f * heroProperties.dexterity) + (1 * heroProperties.stamina) + ( 0.1f * heroProperties.intelligency));
                    heroData.allDefense = 3 * heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.ROGUE:
                    heroProperties.strength = 10;
                    heroProperties.agility = 15;
                    heroProperties.dexterity = 0;
                    heroProperties.stamina = 10;
                    heroProperties.wisdom = 0;
                    heroProperties.intelligency = 0;
                    heroData.damage = (int)((1.5f * heroProperties.strength) + (1.5f * heroProperties.agility));
                    heroData.hitChance = (11 * heroProperties.strength) + (14 * heroProperties.agility) + (3 * heroProperties.wisdom) + (1 * heroProperties.intelligency);
                    heroData.attackSpeed = (20 * heroProperties.strength) + (3 * heroProperties.dexterity);
                    heroData.criticalCance = 15 * heroProperties.agility;
                    heroData.dodge = 0;
                    heroData.life = 12 * heroProperties.stamina;
                    heroData.lifeReg = (int)0.4f * heroProperties.stamina;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.HUNTER:
                    heroProperties.strength = 15;
                    heroProperties.agility = 10;
                    heroProperties.dexterity = 0;
                    heroProperties.stamina = 10;
                    heroProperties.wisdom = 0;
                    heroProperties.intelligency = 0;
                    heroData.damage = (int)((heroProperties.strength * 3.5f) + (heroProperties.agility * 1));
                    heroData.hitChance = (heroProperties.strength * 15) + (heroProperties.agility * 18) + (heroProperties.intelligency * 2);
                    heroData.attackSpeed = heroProperties.strength * 10;
                    heroData.criticalCance = (heroProperties.agility * 20) + (heroProperties.dexterity * 4) + (heroProperties.wisdom * 2);
                    heroData.dodge = 0;
                    heroData.life = heroProperties.stamina * 10;
                    heroData.lifeReg = heroProperties.stamina * (int)0.4f;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.MAGE:
                    heroProperties.strength = 10;
                    heroProperties.agility = 0;
                    heroProperties.dexterity = 10;
                    heroProperties.stamina = 0;
                    heroProperties.wisdom = 15;
                    heroProperties.intelligency = 0;
                    heroData.damage = (int)((heroProperties.strength * 2) + (heroProperties.agility * 0.4f));
                    heroData.hitChance = heroProperties.strength * 8;
                    heroData.attackSpeed = ((heroProperties.strength * 10) + (heroProperties.wisdom * 20));
                    heroData.criticalCance = (heroProperties.wisdom * 20) + (heroProperties.intelligency * 5);
                    heroData.dodge = heroProperties.dexterity * 5;
                    heroData.life = (heroProperties.dexterity * 5) + (heroProperties.stamina * 8) + (heroProperties.stamina * 2);
                    heroData.lifeReg = (int)((heroProperties.dexterity * 0.4f) + (heroProperties.stamina * 0.3f));
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 15 * heroProperties.wisdom;
                    heroData.manaReg = heroProperties.wisdom;
                    heroData.healing = 0;
                    break;
                case HeroClass.PRIEST:
                    heroProperties.strength = 0;
                    heroProperties.agility = 0;
                    heroProperties.dexterity = 0;
                    heroProperties.stamina = 10;
                    heroProperties.wisdom = 10;
                    heroProperties.intelligency = 15;
                    heroData.damage = 0;
                    heroData.hitChance = 0;
                    heroData.attackSpeed = ((heroProperties.strength * 10) + (heroProperties.wisdom * 20));
                    heroData.criticalCance = (heroProperties.wisdom * 15) + (heroProperties.agility * 3) + (heroProperties.dexterity * 2);
                    heroData.dodge = 0;
                    heroData.life = heroProperties.stamina * 6;
                    heroData.lifeReg = 0;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = (10 * heroProperties.wisdom) + (15 * heroProperties.intelligency);
                    heroData.manaReg = heroProperties.intelligency * 2;
                    heroData.healing = (heroProperties.intelligency * 3) + (heroProperties.strength * 1);
                    break;
            }

        }

        private void HeroStatsRefresh()
        {
            switch (heroClass)
            {
                case HeroClass.WARRIOR:
                    heroData.damage = (3 * heroProperties.strength) + (2 * heroProperties.agility);
                    heroData.hitChance = (10 * heroProperties.strength) + (12 * heroProperties.agility) + (3 * heroProperties.dexterity) + (2 * heroProperties.wisdom);
                    heroData.attackSpeed = 15 * heroProperties.strength;
                    heroData.criticalCance = (10 * heroProperties.agility) + (1 * heroProperties.intelligency);
                    heroData.dodge = 0;
                    heroData.life = 15 * heroProperties.stamina + addLife;
                    heroData.lifeReg = (int)0.4f * heroProperties.stamina;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.DEFENDER:
                    heroData.damage = (int)2 * heroProperties.strength;
                    heroData.hitChance = (8 * heroProperties.strength) + (2 * heroProperties.agility);
                    heroData.attackSpeed = 10 * heroProperties.strength;
                    heroData.criticalCance = 0;
                    heroData.dodge = (20 * heroProperties.dexterity) + (3 * heroProperties.wisdom);
                    heroData.life = (15 * heroProperties.dexterity) + (20 * heroProperties.stamina) + addLife;
                    heroData.lifeReg = (int)((1.5f * heroProperties.dexterity) + (1 * heroProperties.stamina) + (0.1f * heroProperties.intelligency));
                    heroData.lifeReg = (int)((1.5f * heroProperties.dexterity) + (1 * heroProperties.stamina) + (0.1f * heroProperties.intelligency));
                    heroData.allDefense = 3 * heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.ROGUE:
                    heroData.damage = (int)((1.5f * heroProperties.strength) + (1.5f * heroProperties.agility));
                    heroData.hitChance = (11 * heroProperties.strength) + (14 * heroProperties.agility) + (3 * heroProperties.wisdom) + (1 * heroProperties.intelligency);
                    heroData.attackSpeed = (20 * heroProperties.strength) + (3 * heroProperties.dexterity);
                    heroData.criticalCance = 15 * heroProperties.agility;
                    heroData.dodge = 0;
                    heroData.life = 12 * heroProperties.stamina + addLife;
                    heroData.lifeReg = (int)0.4f * heroProperties.stamina;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.HUNTER:
                    heroData.damage = (int)((heroProperties.strength * 3.5f) + (heroProperties.agility * 1));
                    heroData.hitChance = (heroProperties.strength * 15) + (heroProperties.agility * 18) + (heroProperties.intelligency * 2);
                    heroData.attackSpeed = heroProperties.strength * 10;
                    heroData.criticalCance = (heroProperties.agility * 20) + (heroProperties.dexterity * 4) + (heroProperties.wisdom * 2);
                    heroData.dodge = 0;
                    heroData.life = heroProperties.stamina * 10 + addLife;
                    heroData.lifeReg = heroProperties.stamina * (int)0.4f;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 0;
                    heroData.manaReg = 0;
                    heroData.healing = 0;
                    break;
                case HeroClass.MAGE:
                    heroData.damage = (int)((heroProperties.strength * 2) + (heroProperties.agility * 0.4f));
                    heroData.hitChance = heroProperties.strength * 8;
                    heroData.attackSpeed = ((heroProperties.strength * 10) + (heroProperties.wisdom * 20));
                    heroData.criticalCance = (heroProperties.wisdom * 20) + (heroProperties.intelligency * 5);
                    heroData.dodge = heroProperties.dexterity * 5;
                    heroData.life = (heroProperties.dexterity * 5) + (heroProperties.stamina * 8) + (heroProperties.stamina * 2) + addLife;
                    heroData.lifeReg = (int)((heroProperties.dexterity * 0.4f) + (heroProperties.stamina * 0.3f));
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = 15 * heroProperties.wisdom;
                    heroData.manaReg = heroProperties.wisdom;
                    heroData.healing = 0;
                    break;
                case HeroClass.PRIEST:
                    heroData.damage = 0;
                    heroData.hitChance = 0;
                    heroData.attackSpeed = ((heroProperties.strength * 10) + (heroProperties.wisdom * 20));
                    heroData.criticalCance = (heroProperties.wisdom * 15) + (heroProperties.agility * 3) + (heroProperties.dexterity * 2);
                    heroData.dodge = 0;
                    heroData.life = heroProperties.stamina * 6 + addLife;
                    heroData.lifeReg = 0;
                    heroData.allDefense = heroProperties.stamina;
                    heroData.mana = (10 * heroProperties.wisdom) + (15 * heroProperties.intelligency);
                    heroData.manaReg = heroProperties.intelligency * 2;
                    heroData.healing = (heroProperties.intelligency * 3) + (heroProperties.strength * 1);
                    break;
            }

        }

        public void UIRefresh()
        {
            HeroStatsRefresh();

            textProperties.text = string.Format("Cast: {9}\nHero: {0} - Level: {1} - Eloszt: {2}\nstrength: {3} - agility: {4} - dexterity: {5}\nStam: {6} - Wisd: {7} - Int: {8}",
                                                 heroName, level, eloszt, heroProperties.strength, heroProperties.agility, heroProperties.dexterity, heroProperties.stamina, heroProperties.wisdom, heroProperties.intelligency, getHeroClass().ToString());

            textData.text = string.Format("damage: {0} - hitChance: {1} - AttackSpeed: {2}\nCriticalCance: {3} - Dodge: {4} - Life{5}\nLifeReg: {6} - AllDef.: {7} - Mana: {8}\nManaReg: {9} - Healing: {10}\n RangedDefense: {11} - MagicalDefense: {12} - MeleeDefense: {13}",
                                           heroData.damage, heroData.hitChance, heroData.attackSpeed, heroData.criticalCance, heroData.dodge, heroData.life, heroData.lifeReg, heroData.allDefense, heroData.mana, heroData.manaReg, heroData.healing,heroData.rangedDefense,heroData.magicalDefense,heroData.meleeDefense);

        }

        public ItemClass getItemClassFromHero()
        {
            switch(heroClass)
            {
                case HeroClass.DEFENDER:
                    return ItemClass.WARRIOR_DEFENDER;
                case HeroClass.WARRIOR:
                    return ItemClass.WARRIOR_DEFENDER;

                case HeroClass.MAGE:
                    return ItemClass.MAGE_PRIEST;

                case HeroClass.ROGUE:
                    return ItemClass.ROGUE_HUNTER;
                case HeroClass.HUNTER:
                    return ItemClass.ROGUE_HUNTER;
                case HeroClass.PRIEST:
                    return ItemClass.MAGE_PRIEST;

                default:
                    return ItemClass.ROGUE_HUNTER;

            }
        }

    }
}
