﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DailyRPG;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//<summary>
//Csak a tesztek alatt.
//</summary>
public class ButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public Item myItem;
    public Image myImage;

    public GameObject toolTip;

    void Start()
    {
        
    }
    public void Upload()
    {
        Hero.getInstance.equipment.AddItemToEquipment(myItem,Callback);
    }
    private void Callback()
    {
        EquipmentUI ui = FindObjectOfType<EquipmentUI>();
        
        myImage.color = Color.red;

        switch(myItem.getArmorType())
        {
            case ArmorType.HELMET:
                ui.helmet.GetComponent<Image>().color = Color.cyan;
                ui.helmet.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.helmet.GetComponent<EquipmentButton>().myItem = myItem;
                break;
            case ArmorType.SHOULDERS:
                ui.shoulders.GetComponent<Image>().color = Color.cyan;
                ui.shoulders.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.shoulders.GetComponent<EquipmentButton>().myItem = myItem;
                break;
            case ArmorType.CHEST:
                ui.chest.GetComponent<Image>().color = Color.cyan;
                ui.chest.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.chest.GetComponent<EquipmentButton>().myItem = myItem;
                break;
            case ArmorType.GLOVES:
                ui.gloves.GetComponent<Image>().color = Color.cyan;
                ui.gloves.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.gloves.GetComponent<EquipmentButton>().myItem = myItem;


                ui.gloves2.GetComponent<Image>().color = Color.cyan;
                ui.gloves2.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.gloves2.GetComponent<EquipmentButton>().myItem = myItem;

                break;
            case ArmorType.LEG:
                ui.leg.GetComponent<Image>().color = Color.cyan;
                ui.leg.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.leg.GetComponent<EquipmentButton>().myItem = myItem;
                break;
            case ArmorType.BOOTS:
                ui.boots.GetComponent<Image>().color = Color.cyan;
                ui.boots.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.boots.GetComponent<EquipmentButton>().myItem = myItem;
                break;
            case ArmorType.BELT:
                ui.belt.GetComponent<Image>().color = Color.cyan;
                ui.belt.transform.GetChild(0).GetComponent<Text>().text = myItem.getName();
                ui.belt.GetComponent<EquipmentButton>().myItem = myItem;
                break;
        }

    }

    private void ToolTipInit(bool isArmor)
    {
        Text text = toolTip.transform.GetChild(0).GetComponent<Text>();

        if (isArmor)
            text.text = "RangedDef: " + myItem.getRangedDefense() + "\nMeleeDef: " + myItem.getMeleeDefense() +"\nMagicDef: " + myItem.getMagicalDefense() + "\nLife: +"+myItem.getLife() + "\nClass: " + myItem.getItemClass() + "\nPrice: " + myItem.getPrice();
    }
    public void OnPointerEnter(PointerEventData data)
    {
        toolTip = EquipmentUI.getInstance.toolTip;
        toolTip.SetActive(true);
        toolTip.transform.position = new Vector2(transform.position.x + 50, transform.position.y + 25);

        switch(myItem.getItemType())
        {
            case Type.ARMOR:
                ToolTipInit(true);
                break;
        }
    }

    public void OnPointerExit(PointerEventData data)
    {
        toolTip = EquipmentUI.getInstance.toolTip;

        toolTip.SetActive(false);
    }
}
