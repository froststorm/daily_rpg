﻿using UnityEngine;
using System.Collections;

public class Rarity : MonoBehaviour {

    int statBonus;
    int valueBonusPercentage;
    int valueBonus;
    int price;

    public void AddRarityType (string typeOfRarity, int itemLevel)
	{
		if (typeOfRarity == "Common") {
			valueBonusPercentage = 0;
			valueBonus = 0;
			statBonus = 0;
			price = itemLevel * 10;
		}
		if (typeOfRarity == "Uncommon") {
			valueBonusPercentage = Random.Range (1, 9);
			valueBonus = 1;
			statBonus = 0;
			price = itemLevel * 15;
		}
		if (typeOfRarity == "Heroic") {
			valueBonusPercentage = Random.Range (3, 10);
			valueBonus = 2;
			statBonus = 1;
			price = itemLevel * 25;
		}
		if (typeOfRarity == "Epic") {
			valueBonusPercentage = Random.Range (6, 15);
			valueBonus = 3;
			statBonus = 2;
			price = itemLevel * 40;
		}
		if (typeOfRarity == "Legendary") {
			valueBonusPercentage = Random.Range (10, 20);
			valueBonus = 4;
			statBonus = 3;
			price = itemLevel * 100;
		}
		if (typeOfRarity == "Common") {
			valueBonusPercentage = Random.Range (20, 25);
			valueBonus = 6;
			statBonus = 3;
			price = itemLevel * 200;
		}
		int bonusDamage = (int)(Random.Range (0.4f, 1.4f * itemLevel));
		int bonusLife = Random.Range (5, 12) * itemLevel;
		int bonusLifeReg = (int)(Random.Range (0.1f, 1) * itemLevel);
		int bonusAttackSpeed = Random.Range (1, 6) * itemLevel;
		int bonusHitChance = Random.Range (3, 12) * itemLevel;
		int bonusDodge = Random.Range (3, 12) * itemLevel;
		int bonusCriticalChance = Random.Range (5, 15) * itemLevel;
		float bonusCriticalDamage = Random.Range (0.50f, 50.00f) * itemLevel;
		int bonusPsychicalDefense = Random.Range (5, 20) * itemLevel;
		int bonusRangedDefense = Random.Range (5, 20) * itemLevel;
		int bonusMagicalDefense = Random.Range (5, 20) * itemLevel;
		int bonusMana = Random.Range (5, 15) * itemLevel;
		int bonusManaReg = Random.Range (5, 15) * itemLevel;
		int bonusHealing = (int)(Random.Range (0.3f, 1.5f) * itemLevel);

		float bonusStat1;
		float bonusStat2;
		float bonusStat3;

		if (statBonus == 1) {
			bonusStat1 = Random.Range (0.4f, 1.5f);
			bonusStat2 = 0;
			bonusStat3 = 0;
		}
		if (statBonus == 2) {
			bonusStat1 = Random.Range (0.4f, 1.5f);
			bonusStat2 = Random.Range (0.3f, 1.2f);
			bonusStat3 = 0;
		}
		if (statBonus == 3) {
			bonusStat1 = Random.Range (0.3f, 1.2f);
			bonusStat2 = Random.Range (0.3f, 1.2f);
			bonusStat3 = Random.Range (0.2f, 1);
		}
	}
}
