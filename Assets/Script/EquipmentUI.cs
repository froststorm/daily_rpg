﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DailyRPG;
using UnityEngine.UI;

public class EquipmentUI : MonoBehaviour
{
    public GameObject helmet;
    public GameObject shoulders;
    public GameObject chest;
    public GameObject gloves;
    public GameObject gloves2;
    public GameObject leg;
    public GameObject boots;
    public GameObject belt;

    public GameObject toolTip;

    public static EquipmentUI getInstance;

    void Start()
    {
        getInstance = this;
    }
}
