﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/*
 * 2016.11.18 *
 * AZ ITEM OSZTÁLYT TUDOD CASTOLNI VELÜK: - ARMORITEM(ARMORS SET BEÁGYAZOTT OSZTÁLYA)
 *                                        - WEAPON
 *|-----------------------------|               
 *| ITEM FEGYVER = (ITEM)WEAPON |
 *| FEGYVER.GETDAMAGE();        |
 *|-----------------------------|
 * 
 * 
 */

namespace DailyRPG
{
    public enum ArmorType
    {
        HELMET,
        SHOULDERS,
        CHEST,
        BELT,
        GLOVES,
        LEG,
        BOOTS

    }
    [System.Serializable]
    public class ArmorItem
    {
        [Header("Name: ")]
        public string _name = "TYPE ITEM NAME...";

        [Header("Life Values: ")]
        public int _life = 0;
        public float _lifeMul = 0.0f;

        [Header("Item Price: ")]
        public int _price;

        [Header("Select Type: ")]
        public ArmorType type;
        public const Type itemType = Type.ARMOR;
        public ItemClass cast;

        [Header("Defense Values: ")]
        public int _meleeDefense = 0;
        public int _rangedDefense = 0;
        public int _magicalDefense = 0;

        [Header("Defense Multiple Values: ")]
        public int _meleeDefenseM = 0;
        public int _rangedDefenseM = 0;
        public int _magicalDefenseM = 0;

        /* KONSTRUKTOR */
        public ArmorItem(ItemClass itemClass,string name, int life, float lifeMul, ArmorType type, int meleeDef, int rangedDef, int magicDef, int meleeDefM, int rangedDefM, int magicalDefM)
        {
            this.cast = itemClass;
            this._name = name;
            this._life = life;
            this._lifeMul = lifeMul;
            this.type = type;

            this._meleeDefense = meleeDef;
            this._rangedDefense = rangedDef;
            this._magicalDefense = magicDef;

            this._meleeDefenseM = meleeDefM;
            this._rangedDefenseM = rangedDefM;
            this._magicalDefenseM = magicalDefM;
        }

        public string getName()
        {
            return _name;
        }
        public int getLife()
        {
            return _life;
        }
        public float getLifeMul()
        {
            return _lifeMul;
        }

        public int getMeleeDefense()
        {
            return _meleeDefense;
        }
        public int getRangedDefense()
        {
            return _rangedDefense;
        }
        public int getMagicalDefense()
        {
            return _magicalDefense;
        }
        public int getPrice()
        {
            return _price;
        }
        public int getSellPrice()
        {
            return _price / 5;
        }

        public ArmorType getType()
        {
            return type;
        }
        public Type getItemType()
        {
            return itemType;
        }
        public Scematic getScematic()
        {
            Scematic scematic = new Scematic(getName(), getPrice());

            return scematic;
        }
        public ItemClass getClass()
        {
            return cast;
        }
        public bool CheckCast(ItemClass classItem)
        {
            Hero hero = Hero.getInstance;
            return getClass() == hero.getItemClassFromHero();
        }
    }

    [System.Serializable]
    public class ArmorSet
    {
        public string _name = "TYPE SET NAME...";
        public ItemClass cast;

        public ArmorItem helmet;
        public ArmorItem shoulders;
        public ArmorItem chest;
        public ArmorItem belt;
        public ArmorItem gloves;
        public ArmorItem leg;
        public ArmorItem boots;

        public ArmorSet(string name,ArmorItem helmet, ArmorItem shoulders, ArmorItem chest, ArmorItem belt, ArmorItem gloves, ArmorItem leg, ArmorItem boots)
        {
            this._name = name;
            this.helmet = helmet;
            this.shoulders = shoulders;
            this.chest = chest;
            this.belt = belt;
            this.gloves = gloves;
            this.leg = leg;
            this.boots = boots;
        }

        #region GETTERS

        public string getName()
        {
            return _name;
        }
        public ArmorItem getHelmet()
        { return helmet; }
        public ArmorItem getSholuders()
        { return shoulders; }
        public ArmorItem getChest()
        { return chest; }
        public ArmorItem getBelt()
        { return belt; }
        public ArmorItem getGloves()
        { return gloves; }
        public ArmorItem getLeg()
        { return leg; }
        public ArmorItem getBoots()
        { return boots; }
        
        public int getMeleeDefense()
        {
            int i = 0;
            List<ArmorItem> items = new List<ArmorItem>() { helmet, shoulders, chest, belt, gloves, leg, boots };
            for(int x = 0; x < items.Count; x++)
            {
                i += items[x].getMeleeDefense();
            }

            return i;
        }
        
        public List<ArmorItem> getArmorItems()
        {
            List<ArmorItem> items = new List<ArmorItem> { helmet, shoulders, chest, belt, gloves, leg, boots };
            return items;
        }

        #endregion

    }

    public class Armors : MonoBehaviour
    {
        public List<ArmorSet> set = new List<ArmorSet>();
    }
    
}

