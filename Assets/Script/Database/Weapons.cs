﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DailyRPG
{
    public enum WeaponCast
    {
        HUNTER,
        ROGUE,
        WARRIOR,
        DEFENDER,
        MAGE,
        PRIEST
    }
    public enum WeaponType
    {
        RANGED,
        MELEE,
        MAGICIAL
    }
    public enum Hand
    {
        TWO_HANDED,
        ONE_HANDED
    }

    [System.Serializable]
    public class Weapon 
    {
        [Header("Name: ")]
        public string _name = "TYPE ITEM NAME...";

        [Header("Information: ")]
        [Multiline]
        public string _information = "TYPE INFORMATION...";

        [Header("Price: ")]
        public int _price;

        [Header("Select Cast: ")]
        public WeaponCast weaponCast;

        [Header("Select Weapon Type: ")]
        public WeaponType type;
        public const Type itemType = Type.WEAPON;

        [Header("Select Hand Status: ")]
        public Hand hand;

        [Header("Damage Values: ")]
        public int _minDamage = 0;
        public int _maxDamage = 0;

        [Header("Damage Multiple Values: ")]
        public int _minDamageM = 0;
        public int _maxDamageM = 0;

        [Header("Healing Values: ")]
        public float _minHeal;
        public float _maxHeal;

        [Header("Healing Multipler Values: ")]
        public float _minHealM;
        public float _maxHealM;

        [Header("Attack Speed: ")]
        public float _attackSpeed = 0.0f;

        public Scematic scematic;

        public Weapon( string name, int price, Scematic scematic, WeaponCast cast, WeaponType type, Hand hand, int minD, int maxD, float minH, float maxH)
        {
            this._name = name;
            this._price = price;
            this.scematic = scematic;
            this.weaponCast = cast;
            this.type = type;
            this.hand = hand;
            this._minDamage = minD;
            this._maxDamage = maxD;
            this._minHeal = minH;
            this._maxHeal = maxH;
        }
        public string getName()
        {
            return _name;
        }
        public string getInformation()
        {
            return _information;
        }

        public int getPrice()
        {
            return _price;
        }
        public int getSellPrice()
        {
            return _price / 5;
        }

        public Type getItemType()
        {
            return itemType;
        }
        public WeaponCast getCast()
        {
            return weaponCast;
        }
        public WeaponType getType()
        {
            return type;
        }
        public Hand getHandType()
        {
            return hand;
        }

        public int getMinDamage()
        {
            return _minDamage;
        }
        public int getMaxDamage()
        {
            return _maxDamage;
        }

        public int getMaxDamageMultiplier()
        {
            return _maxDamageM;
        }
        public int getMinDamageMultiplier()
        {
            return _minDamageM;
        }

        public float getMinHeal()
        {
            return _minHeal;
        }
        public float getMaxHeal()
        {
            return _maxHeal;
        }

        public float getMinHealMultipler()
        {
            return _minHealM;
        }
        public float getMaxHealMultipler()
        {
            return _maxHealM;
        }

        public float getAttackSpeed()
        {
            return _attackSpeed;
        }

        public Weapon getWeapon()
        {
            return this;
        }

        public Scematic getScematic()
        {
            Scematic scematic = new Scematic(getName(), getPrice());

            return scematic;
        }
        

    }
    public class Weapons : MonoBehaviour
    {
        public List<Weapon> weapons = new List<Weapon>();
    }
}
